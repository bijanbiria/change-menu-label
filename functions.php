function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'موسسات همکار';
	$submenu['edit.php'][5][0] = 'موسسات همکار';
	$submenu['edit.php'][10][0] = 'افزودن موسسه همکار';
	echo '';
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'موسسات همکار';
	$labels->singular_name = 'موسسات همکار';
	$labels->add_new = 'افزودن موسسه همکار';
	$labels->add_new_item = 'افزودن موسسه همکار';
	$labels->edit_item = 'ویرایش موسسات';
	$labels->new_item = 'موسسه همکار';
	$labels->view_item = 'مشاهده موسسه همکار';
	$labels->search_items = 'جستجو در موسسات';
	$labels->not_found = 'موسسه ای یافت نشد';
	$labels->not_found_in_trash = 'موسسه ای در زباله دان یافت نشد';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );